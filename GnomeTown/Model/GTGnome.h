//
//  GTGnome.h
//  GnomeTown
//
//  Created by ThXou on 27/11/2016.
//  Copyright © 2016 ThXou. All rights reserved.
//

@interface GTGnome : NSObject

@property (nonatomic, copy) NSString *hairColor;
@property (nonatomic, copy) NSString *name;
@property (nonatomic) NSInteger gnomeID;
@property (nonatomic) NSInteger age;
@property (nonatomic) CGFloat weight;
@property (nonatomic) CGFloat height;
@property (nonatomic) NSURL *thumbnailURL;
@property (nonatomic) NSArray *professions;
@property (nonatomic) NSArray *friends;

+ (GTGnome *)importFromDictionary:(NSDictionary *)dictionary;

@end
