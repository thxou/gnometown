//
//  GTGnome.m
//  GnomeTown
//
//  Created by ThXou on 27/11/2016.
//  Copyright © 2016 ThXou. All rights reserved.
//

#import "GTGnome.h"

@implementation GTGnome

+ (GTGnome *)importFromDictionary:(NSDictionary *)dictionary
{
    GTGnome *gnome = [[GTGnome alloc] init];
    [gnome importValuesFromDictionary:dictionary];
    return gnome;
}


- (void)importValuesFromDictionary:(NSDictionary *)dictionary
{
    self.gnomeID = [dictionary[@"id"] integerValue];
    self.name = dictionary[@"name"];
    self.thumbnailURL = [NSURL URLWithString:dictionary[@"thumbnail"]];
    self.age = [dictionary[@"age"] integerValue];
    self.weight = [dictionary[@"weight"] doubleValue];
    self.height = [dictionary[@"height"] doubleValue];
    self.hairColor = dictionary[@"hair_color"];
    self.professions = dictionary[@"professions"];
    self.friends = dictionary[@"friends"];
}


- (NSString *)description {
    return [NSString stringWithFormat:@"name: %@ | age: %li | hair color: %@ | weight: %f | height: %f",
            self.name,
            (long)self.age,
            self.hairColor,
            self.weight,
            self.height ];
}



@end
