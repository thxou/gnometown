//
//  GnomeDetailViewController.m
//  GnomeTown
//
//  Created by ThXou on 27/11/2016.
//  Copyright © 2016 ThXou. All rights reserved.
//

#import "GnomeDetailViewController.h"
#import "GnomeCell.h"
#import "GTGnome.h"


@interface GnomeDetailViewController ()

@end


@implementation GnomeDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    self.title = @"Gnome DNI";
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
        return self.gnome.professions.count > 0 ? self.gnome.professions.count : 1;
    } else if (section == 2) {
        return self.gnome.friends.count > 0 ? self.gnome.friends.count : 1;
    } else {
        return 1;
    }
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return @"Professions";
    } else if (section == 2) {
        return @"Friends";
    }
    return @"";
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        GnomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GnomeDetailCell" forIndexPath:indexPath];
        [cell configureCellWithGnome:self.gnome];
        return cell;
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GnomeGeneralCell" forIndexPath:indexPath];
        cell.textLabel.font = [UIFont fontWithName:@"MonotypeGurmukhi" size:16.0];
        
        if (indexPath.section == 1)
        {
            if (self.gnome.professions.count > 0) {
                cell.textLabel.text = self.gnome.professions[indexPath.row];
            } else {
                cell.textLabel.textColor = [UIColor lightGrayColor];
                cell.textLabel.text = @"How idle is this gnome!";
            }
        }
        else
        {
            if (self.gnome.friends.count > 0) {
                cell.textLabel.text = self.gnome.friends[indexPath.row];
            }
            else
            {
                cell.textLabel.textColor = [UIColor lightGrayColor];
                cell.textLabel.text = @"This gnome need a friend ☹!";
            }
        }
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}



@end
