//
//  GnomesViewController.m
//  GnomeTown
//
//  Created by ThXou on 27/11/2016.
//  Copyright © 2016 ThXou. All rights reserved.
//

#import "GnomesViewController.h"
#import "GnomeDetailViewController.h"
#import "GnomeCell.h"
#import "GTGnome.h"


@interface GnomesViewController () <UISearchResultsUpdating, UISearchBarDelegate>

@property (nonatomic) NSArray *filteredGnomes;
@property (nonatomic) UISearchController *searchController;
@property (nonatomic) NSIndexPath *selectedIndexPath;

@end


@implementation GnomesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.title = @"Brastlewark inhabitants";
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.scopeButtonTitles = @[ @"Name", @"Hair Color" ];
    self.searchController.searchBar.delegate = self;
    self.searchController.searchBar.tintColor = [UIColor darkGrayColor];
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 111.0;
}



#pragma mark - UISearchBar delegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searchController.active = NO;
    [self.tableView reloadData];
}


- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    [self filterContentForSearchText:searchBar.text scope:searchBar.scopeButtonTitles[selectedScope]];
}



#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    UISearchBar *searchBar = searchController.searchBar;
    NSString *scope = searchBar.scopeButtonTitles[searchBar.selectedScopeButtonIndex];
    [self filterContentForSearchText:searchController.searchBar.text scope:scope];
}


- (void)filterContentForSearchText:(NSString *)text scope:(NSString *)scope
{
    NSPredicate *predicate;
    if ([scope isEqualToString:@"Hair Color"]) {
        predicate = [NSPredicate predicateWithFormat:@"hairColor contains[cd] %@", self.searchController.searchBar.text];
    }
    else {
        predicate = [NSPredicate predicateWithFormat:@"SELF.%@ contains[cd] %@", scope.lowercaseString, self.searchController.searchBar.text];
    }

    self.filteredGnomes = [self.gnomes filteredArrayUsingPredicate:predicate];
    [self.tableView reloadData];
}



#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.searchController.active && ![self.searchController.searchBar.text isEqualToString:@""]) {
        return self.filteredGnomes.count;
    }
    return self.gnomes.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GTGnome *gnome = self.gnomes[indexPath.row];
    if (self.searchController.active && ![self.searchController.searchBar.text isEqualToString:@""]) {
        gnome = self.filteredGnomes[indexPath.row];
    } else {
        gnome = self.gnomes[indexPath.row];
    }
    
    GnomeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GnomeCell" forIndexPath:indexPath];
    [cell configureCellWithGnome:gnome];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.searchController.searchBar.text = @"";
    [self.searchController.searchBar resignFirstResponder];
    self.searchController.active = NO;
    
    self.selectedIndexPath = indexPath;
    [self performSegueWithIdentifier:@"ToGnomeDetail" sender:nil];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ToGnomeDetail"])
    {
        GTGnome *gnome;
        if (self.searchController.active && ![self.searchController.searchBar.text isEqualToString:@""]) {
            gnome = self.filteredGnomes[self.selectedIndexPath.row];
        } else {
            gnome = self.gnomes[self.selectedIndexPath.row];
        }
        
        GnomeDetailViewController *controller = segue.destinationViewController;
        controller.gnome = gnome;
    }
}


@end
