//
//  GnomesViewController.h
//  GnomeTown
//
//  Created by ThXou on 27/11/2016.
//  Copyright © 2016 ThXou. All rights reserved.
//

@interface GnomesViewController : UITableViewController

@property (nonatomic) NSArray *gnomes;

@end
