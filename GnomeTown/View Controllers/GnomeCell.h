//
//  GnomeCell.h
//  GnomeTown
//
//  Created by ThXou on 27/11/2016.
//  Copyright © 2016 ThXou. All rights reserved.
//


@class GTGnome;
@interface GnomeCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *thumbnailImageView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *ageLabel;
@property (nonatomic, weak) IBOutlet UILabel *weightLabel;
@property (nonatomic, weak) IBOutlet UILabel *heightLabel;
@property (nonatomic, weak) IBOutlet UILabel *friendsLabel;
@property (nonatomic, weak) IBOutlet UIView *hairColor;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *thumbnailHeightConstraint;

- (void)configureCellWithGnome:(GTGnome *)gnome;

@end
