//
//  GnomeDetailViewController.h
//  GnomeTown
//
//  Created by ThXou on 27/11/2016.
//  Copyright © 2016 ThXou. All rights reserved.
//

@class GTGnome;
@interface GnomeDetailViewController : UITableViewController

@property (nonatomic) GTGnome *gnome;

@end
