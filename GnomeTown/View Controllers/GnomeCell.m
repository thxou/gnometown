//
//  GnomeCell.m
//  GnomeTown
//
//  Created by ThXou on 27/11/2016.
//  Copyright © 2016 ThXou. All rights reserved.
//

#import "GnomeCell.h"
#import "GTGnome.h"
#import <UIImageView+WebCache.h>


@implementation GnomeCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.thumbnailImageView.layer.cornerRadius = 5.0;
    self.thumbnailImageView.layer.masksToBounds = YES;
    
    self.hairColor.layer.cornerRadius = self.hairColor.frame.size.width / 2;
    self.hairColor.layer.borderWidth = 1.0;
    self.hairColor.layer.borderColor = [UIColor darkGrayColor].CGColor;
}


- (void)configureCellWithGnome:(GTGnome *)gnome
{
    self.nameLabel.text = gnome.name;
    self.ageLabel.text = [NSString stringWithFormat:@"%li y", (long)gnome.age];
    self.weightLabel.text = [NSString stringWithFormat:@"%.01f kg", gnome.weight];
    self.heightLabel.text = [NSString stringWithFormat:@"%.01f cm", gnome.height];
    self.hairColor.backgroundColor = [self colorWithText:gnome.hairColor];
    self.thumbnailImageView.image = [UIImage imageNamed:@"placeholder"];
    
    
    if (gnome.friends.count > 0) {
        self.friendsLabel.text = [NSString stringWithFormat:@"%li", (long)gnome.friends.count];
    } else {
        self.friendsLabel.text = @"☹";
    }
    
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:gnome.thumbnailURL
                          options:0
                         progress:nil
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                            if (image) {
                                CGFloat height = self.thumbnailImageView.frame.size.width / image.size.width * image.size.height;
                                self.thumbnailHeightConstraint.constant = height < 92 ? height : 92;
                                self.thumbnailImageView.image = image;
                                [self layoutIfNeeded];
                            }
                        }];
}


- (UIColor *)colorWithText:(NSString *)text
{
    if ([text isEqualToString:@"Pink"]) {
        return [UIColor colorWithRed:238.0/255.0 green:127.0/255.0 blue:255.0/255.0 alpha:1.0];
    } else if ([text isEqualToString:@"Green"]) {
        return [UIColor greenColor];
    } else if ([text isEqualToString:@"Red"]) {
        return [UIColor redColor];
    } else if ([text isEqualToString:@"Black"]) {
        return [UIColor blackColor];
    } else if ([text isEqualToString:@"Gray"]) {
        return [UIColor grayColor];
    }
    return [UIColor whiteColor];
}



@end
