//
//  HomeViewController.m
//  GnomeTown
//
//  Created by ThXou on 23/11/2016.
//  Copyright © 2016 ThXou. All rights reserved.
//

#import "HomeViewController.h"
#import "GnomesViewController.h"
#import "GTGnome.h"

static NSString *const kResourseURL = @"https://raw.githubusercontent.com/AXA-GROUP-SOLUTIONS/mobilefactory-test/master/data.json";

@interface HomeViewController () <NSURLSessionDownloadDelegate>

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, weak) IBOutlet UILabel *messagesLabel;
@property (nonatomic, weak) IBOutlet UILabel *populationTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *populationLabel;
@property (nonatomic, weak) IBOutlet UILabel *ageAverageTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *ageAverageLabel;
@property (nonatomic, weak) IBOutlet UIButton *seeTheGnomesButton;

@property (nonatomic) NSURLSessionDownloadTask *downloadTask;
@property (nonatomic) NSURLSession *backgroundSession;
@property (nonatomic) NSMutableArray *gnomes;

@end


@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"sessionConfiguration"];
    self.backgroundSession = [NSURLSession sessionWithConfiguration:sessionConfiguration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    [self.activityIndicator startAnimating];
    
    
    // start downloading
    
    NSURL *resourseURL = [NSURL URLWithString:kResourseURL];
    self.downloadTask = [self.backgroundSession downloadTaskWithURL:resourseURL];
    [self.downloadTask resume];
}



#pragma mark - Helpers

- (void)showAlertWithMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"GnomeTown" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


- (NSURL *)savedDataURL {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = paths[0];
    return [NSURL fileURLWithPath:[documentsPath stringByAppendingPathComponent:@"data.json"]];
}


- (void)parseFile
{
    [self.navigationItem setPrompt:@"The Gnome Town where heroes love to stay"];
    
    [UIView transitionWithView:self.messagesLabel
                      duration:0.5
                       options:UIViewAnimationOptionBeginFromCurrentState
                    animations:^{
                        self.messagesLabel.text = @"Parking the heroes' horses...";
                    } completion:nil];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSError *error = nil;
        NSArray *gnomes;
        
        NSURL *destinationURL = [self savedDataURL];
        NSData *data = [NSData dataWithContentsOfURL:destinationURL];
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
        
        if (!error) {
            gnomes = response[@"Brastlewark"];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (error) {
                [self showAlertWithMessage:error.localizedDescription];
            }
            else
            {
                [self parseGnomes:gnomes];
                
                NSNumber *average = [gnomes valueForKeyPath:@"@avg.age.intValue"];
                NSInteger population = gnomes.count;
                
                self.ageAverageLabel.text = [NSString stringWithFormat:@"%li", (long)[average intValue]];
                self.populationLabel.text = [NSString stringWithFormat:@"%li", (long)population];
                [self animateLabels];
            }
            
        });
        
    });
}


- (void)parseGnomes:(NSArray *)gnomes
{
    self.gnomes = [@[] mutableCopy];
    
    for (NSDictionary *gnomeDictionary in gnomes) {
        GTGnome *gnome = [GTGnome importFromDictionary:gnomeDictionary];
        [self.gnomes addObject:gnome];
    }
}


- (void)animateLabels
{
    [UIView transitionWithView:self.messagesLabel
                      duration:0.5
                       options:UIViewAnimationOptionBeginFromCurrentState
                    animations:^{
                        self.messagesLabel.text = @"";
                    } completion:nil];
    
    
    [UIView animateWithDuration:0.5 animations:^{
        self.populationTitleLabel.alpha = 1.0;
        self.ageAverageTitleLabel.alpha = 1.0;
        self.seeTheGnomesButton.alpha = 1.0;
    }];
    
    [UIView animateWithDuration:1.0 animations:^{
        self.populationLabel.alpha = 1.0;
        self.ageAverageLabel.alpha = 1.0;
    }];
}



#pragma mark - NSURLSession Delegate

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    [self.activityIndicator stopAnimating];
    
    NSURL *destinationURL = [self savedDataURL];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    if ([fileManager fileExistsAtPath:[destinationURL path]]) {
        [fileManager removeItemAtURL:destinationURL error:&error];
        
        if (error) {
            [self showAlertWithMessage:error.localizedDescription];
            return;
        }
    }
    
    if ([fileManager moveItemAtURL:location toURL:destinationURL error:&error]) {
        [self parseFile];
    } else {
        [self showAlertWithMessage:error.localizedDescription];
    }
}


- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    [self.activityIndicator stopAnimating];
    self.downloadTask = nil;
    
    if (error) {
        [self showAlertWithMessage:error.localizedDescription];
    }
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ToGnomes"])
    {
        GnomesViewController *controller = segue.destinationViewController;
        controller.gnomes = self.gnomes;
    }
}



@end
